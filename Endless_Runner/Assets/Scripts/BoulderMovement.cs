﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderMovement : MonoBehaviour {

        private Rigidbody2D obstacle;

    public float speed = -3.0f;
    public float rockLowerLimit = -4;
    public float rockHigherLimit = -1;


	// Use this for initialization
	void Start ()
    {
        obstacle = GetComponent<Rigidbody2D>();

        Invoke("SpawnObstacle", 4);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void SpawnObstacle()
    {
        float height = Random.Range(rockLowerLimit, rockHigherLimit);
        Vector3 pos = new Vector3(10, height, 0);
        Quaternion rot = Quaternion.Euler(new Vector3(0, 0, 0));

        Rigidbody2D obstacleInstance = Instantiate(obstacle, pos, rot);

        obstacleInstance.name = "Obstacle(Clone)";

        obstacleInstance.velocity = new Vector2(speed, 0);


    }

}
