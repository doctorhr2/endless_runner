﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Animator animator;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Fixed Update called in sync with the physic engine (fixed rate per second). 
    void FixedUpdate()
    {
        MoveCharacter(); 
    }

    void MoveCharacter()
    {
        if(Input.GetKey(KeyCode.W))
        {
            animator.SetBool("jump", true);
        }
        else
        {
            animator.SetBool("jump", false);  
        }

        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("slide", true);
        }
        else
        {
            animator.SetBool("slide", false);
        }
    }
}
